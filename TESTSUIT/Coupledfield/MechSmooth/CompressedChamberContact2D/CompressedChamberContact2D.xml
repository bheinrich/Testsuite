<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
 
  <documentation>
    <title>CompressedChamberContact2D</title>
    <authors>
      <author>Dominik Mayrhofer</author>
    </authors>
    <date>2022-12-02</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>flow</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      This test consists of a metal plate connected with a volume filled with air. The plate is excited with a pressure BC. 
      First the mechanicPDE is calculated using the pressure BC. Here, Dirichlet BCs are used to restrict movement only to the x-axis. The quantity 'mechDisplacement' is used to couple the mechanic domain to the smoothPDE at the interface. 
      For the smoothPDE a contact law is described via the mathParser. This displacement dependent contact force scales linearly with the displacement and starts to act when the gap between the two compressed surfaces is below a certain threshold. Since the system is undamped, the plate simply bounces back. 
      In order to achieve a convergent solution, we define an iterative coupling with corresponding stopping criteria within the couplingList.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <!--<cdb fileName="CompressedChamber2D.cdb"/>-->
      <hdf5 fileName="CompressedChamberContact2D.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="w_plate" value="3e-4"/>
      <var name="w_air" value="1e-3"/>
      <var name="h" value="1e-3"/>
      <var name="gap" value="2e-4"/>
    </variableList>
    
    <regionList>
      <region name="plate" material="silizium"/>
      <region name="Air" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="ExcPre"/>
      <surfRegion name="Fix_t"/>
      <surfRegion name="Fix_b"/>
      <surfRegion name="PlateAirCoupling"/>
      <surfRegion name="Fix_Air_t"/>
      <surfRegion name="Fix_Air_b"/>
      <surfRegion name="Fix_Air_r"/>
    </surfRegionList>
  </domain>
  
  <sequenceStep index="1"> 
    <analysis>
      <transient>
        <numSteps>25</numSteps>
        <deltaT>2e-4</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <mechanic subType="planeStrain">
      	<regionList>
          <region name="plate"/>
        </regionList>
        
        <bcsAndLoads>
          <!-- Fix the top and bottom interface to enabling sliding in x-direction -->
          <fix name="Fix_b">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_t">
            <comp dof="y"/>
          </fix>
          <!-- We excite the plate with some pressure to accelerate it in positive x-direction -->
          <pressure name="ExcPre" value="200*(1-exp(-t/(2e-4)))"/>
          <!-- After the air is compressed far enough so that the contact law starts acting, the plate is pushed back again by the contact force -->
          <traction name="PlateAirCoupling">
            <coupling pdeName="smooth">
            	<quantity name="smoothContactForceDensity"/>
            </coupling>
          </traction>
        </bcsAndLoads>
         
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <surfElemResult type="mechNormalStress">
          </surfElemResult>
        </storeResults>
      </mechanic>
      
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air"/>
        </regionList>
        
        <contactList>
          <contact Surface1="PlateAirCoupling" Surface2="Fix_Air_r" Volume="Air" contactLaw="(gap-u gt 0)? ((gap-u)/gap*3000) : 0" />
        </contactList>
        
        <bcsAndLoads>
          <!-- Fix the top and bottom interface to enabling sliding in x-direction -->
          <fix name="Fix_Air_t">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_Air_b">
            <comp dof="y"/>
          </fix>
          <!-- Completely fix the right interface (wall) -->
          <fix name="Fix_Air_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <!-- The left interface is (forward-) coupled to the mechPDE by prescribing the displacement directly - the back-coupling happens via the contact force -->
          <displacement name="PlateAirCoupling">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement"/>
            </coupling>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <surfElemResult type="smoothContactForceDensity">
            <surfRegionList>
              <surfRegion name="PlateAirCoupling"/>
              <surfRegion name="Fix_Air_r" />
            </surfRegionList>
          </surfElemResult>
          <surfRegionResult type="smoothContactForce">
            <surfRegionList>
              <surfRegion name="PlateAirCoupling" outputIds="h5,txt"/>
              <surfRegion name="Fix_Air_r" outputIds="h5,txt"/>
            </surfRegionList>
          </surfRegionResult>
        </storeResults>
      </smooth>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="mechanic;smooth">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="mechDisplacement" value="1e-3" normType="rel"/>
          <quantity name="mechVelocity" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Air"/>
          <region name="plate"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
  </sequenceStep>
  
</cfsSimulation>
