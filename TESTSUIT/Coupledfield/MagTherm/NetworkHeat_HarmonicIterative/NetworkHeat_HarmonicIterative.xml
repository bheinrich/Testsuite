<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Magnetic Network coupled with Heat Condution</title>
        <authors>
            <author>ftoth</author>
        </authors>
        <date>2018-09-08</date>
        <keywords>
            <keyword>magneticEdge</keyword>
            <keyword>heatConduction</keyword>
            <keyword>harmonic</keyword>
        </keywords>
        <references> </references>
        <isVerified>no</isVerified>
        <description> 
Simple magnetic network of a conducting wire though a magnetic core.
The BCs are set such that magnetic flux is possible only in z-direction.
The wire is excited by current in x-direction.

We selcect the geometry such that we have unit-areas and lengths.

The heat PDE is made static by setting the heat capacity to 0.
On the sides (T,B,E,W) we specify 0 heat-flow, thus we get a 1D problem.
Heat transfer boundary conditions on the top set the temperature (by alpha).
Heat conductivity is so high, that an almost constant temperature is obtained.

The electric conductivity in the exciting wire (where also eddy currents are induced)
is temperature dependent (ElecCond.fnc).

For a comparison with the analysic solution see the iPython notebook.
        </description>
    </documentation>
    <fileFormats>
        <input>
            <hdf5 fileName="NetworkHeat_HarmonicIterative.h5ref"/>
            <!--<gmsh fileName="SimpleTrafo.msh"/>-->
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d" printGridInfo="yes">
        <variableList>
            <var name="a" value="1.0"/>
            <var name="b1" value="1.0"/>
            <var name="b2" value="1.0"/>
            <var name="b3" value="b1"/>
            <var name="c" value="1.0"/>
            <var name="I_coil" value="1.0"/>
            <var name="alpha" value="1.0e-4"/>
        </variableList>
        <regionList>
            <region name="V1" material="SimpleCore"/>
            <region name="V3" material="SimpleCore"/>
            <region name="V2" material="SimpleCoil"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    
    <sequenceStep index="1">
        <analysis>
            <!--<static/>-->
            <harmonic>
                <frequencyList>
                    <freq value="0.2"/>
                    <freq value="0.5"/>
                    <freq value="1.0"/>
                </frequencyList>
            </harmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V1" polyId="Hcurl"/>
                    <region name="V2" polyId="Hcurl" matDependIds="cond"/>
                    <region name="V3" polyId="Hcurl"/>
                </regionList>
                
                <matDependencyList>
                    <electricConductivity id="cond">
                        <coupling pdeName="heatConduction">
                            <quantity name="heatMeanTemperature"/>
                        </coupling>
                    </electricConductivity>
                </matDependencyList>
                
                <bcsAndLoads>
                    <fluxParallel name="S1W"/>
                    <fluxParallel name="S2W"/>
                    <fluxParallel name="S3W"/>
                    <fluxParallel name="S1O"/>
                    <fluxParallel name="S2O"/>
                    <fluxParallel name="S3O"/>
                    <fluxParallel name="S1S"/>
                    <fluxParallel name="S3N"/>
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil">
                        <source type="current" value="I_coil"/>
                        <part id="1">
                            <regionList>
                                <region name="V2"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="x" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="b2*c"/>
                        </part>
                    </coil>
                </coilList>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magCoilCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotentialD1">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>                    
                    <surfRegionResult type="magFlux">
                        <surfRegionList>
                            <surfRegion name="S1W" outputIds="txt"/>
                            <surfRegion name="S1O" outputIds="txt"/>
                            <surfRegion name="S2W" outputIds="txt"/>
                            <surfRegion name="S2O" outputIds="txt"/>
                            <surfRegion name="S3W" outputIds="txt"/>
                            <surfRegion name="S3O" outputIds="txt"/>
                            <surfRegion name="S1T" outputIds="txt"/>
                            <surfRegion name="S2T" outputIds="txt"/>
                            <surfRegion name="S3T" outputIds="txt"/>
                            <surfRegion name="S1B" outputIds="txt"/>
                            <surfRegion name="S2B" outputIds="txt"/>
                            <surfRegion name="S3B" outputIds="txt"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <coilResult type="coilInducedVoltage">
                        <coilList>
                            <coil id="coil" outputIds="txt"/>
                        </coilList>
                    </coilResult>
                </storeResults>
            </magneticEdge>
            
            <heatConduction>
                <regionList>
                    <region name="V1" polyId="H1"/>
                    <region name="V2" polyId="H1"/>
                    <region name="V3" polyId="H1"/>
                </regionList>               
                
                <bcsAndLoads>
                    <heatSourceDensity volumeRegion="V2" name="V2">
                        <coupling pdeName="magneticEdge">
                            <quantity name="magJouleLossPowerDensity"/>
                        </coupling>
                    </heatSourceDensity>
                    <heatTransport name="S1S" volumeRegion="V1" bulkTemperature="0" heatTransferCoefficient="alpha"/>
                    <heatTransport name="S3N" volumeRegion="V3" bulkTemperature="0" heatTransferCoefficient="alpha"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                    <nodeResult type="heatRhsLoad">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="heatFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <surfRegionResult type="heatFlux">
                        <surfRegionList>
                            <surfRegion name="S12" outputIds="txt"/>
                            <surfRegion name="S23" outputIds="txt"/>
                            <surfRegion name="S1S" outputIds="txt"/>
                            <surfRegion name="S3N" outputIds="txt"/>
                        </surfRegionList>
                    </surfRegionResult>
                </storeResults>
            </heatConduction>
        </pdeList>
        
        <couplingList>
            <iterative>
                <convergence logging="yes" maxNumIters="10" stopOnDivergence="yes">
                    <quantity name="magJouleLossPowerDensity" value="1e-5" normType="rel"/>
                </convergence>
            </iterative>
        </couplingList>
            
        
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes" method="fixPoint">
                            <lineSearch/>
                            <incStopCrit> 1e-3</incStopCrit>
                            <resStopCrit> 1e-3</resStopCrit>
                            <maxNumIters> 20  </maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>
