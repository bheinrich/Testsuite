Point(1) = {0, 0, 0, 1e+22};
Point(2) = {2, 0, 0, 1e+22};
Point(3) = {2, 2, 0, 1e+22};
Point(4) = {0, 2, 0, 1e+22};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};
Physical Surface("internal") = {6};
Physical Line("bottom") = {1};
Physical Line("left") = {4};
Physical Line("top") = {3};
Physical Line("right") = {2};
