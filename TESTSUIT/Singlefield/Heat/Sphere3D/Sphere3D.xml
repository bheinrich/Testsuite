<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Heat PDE with Dirichlet BC's</title>
    <authors>
      <author>F. Toth</author>
    </authors>
    <date>2018-08-18</date>
    <keywords>
      <keyword>heatConduction</keyword>
      <keyword>static</keyword>
      <keyword>volumeSrc</keyword>
      <keyword>temperature</keyword>
    </keywords>
    <references></references>
    <isVerified>yes</isVerified>
    <description>
      We consider 1/8 of a unit sphere with a constant heat source density q.
      On the surface of the sphere we define a Robin BC (heatTransport) with a
      heat transfer coefficient a and a reference tempertature of 0 (thus dT=T).
      
      Thus we can compute the surface temperature from a heat balance:
      Produced heat: Pin = V*q = 1/8*4/3*pi*R^3 * q = 1/6*pi*q
      Transfered heat: Pout = S*a*dT = 1/8*4*pi*R^2 * a * dT = 1/2*pi*a*T
      Pin = Pout -> T = 1/3 * q/a
      
      Setting q=6/pi should gives Pin=1,
      and setting a=2/pi should give T=1.
      
      We compare the temperature at the surface T, and the total heat flux trough the surface.
      The temperture is fairly accurate.
      The total heat flux is mesh-dependent and converges slowly towards the analytical value of 1.
    </description>
  </documentation>
  <fileFormats>
    <input>
      <!--cdb fileName="UnitSphereEighth.cdb"/-->
      <hdf5 fileName="Sphere3D.h5ref"/>
    </input>
    <output>
      <hdf5/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d">
    <regionList>
      <region name="vol" material="one"></region>
    </regionList> 
    <nodeList>
      <nodes name="Rx">
        <coord x="1" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  
  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>    
      <heatConduction>
        <regionList>
          <region name="vol"/>
        </regionList>

        <bcsAndLoads>
          <heatSourceDensity name="vol" value="6/pi"/>
          <heatTransport name="surf" heatTransferCoefficient="2/pi" bulkTemperature="0" volumeRegion="vol"/>          
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
            <nodeList>
              <nodes name="Rx" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="heatFluxDensity">
            <allRegions/>
          </elemResult>
          <surfElemResult type="heatFluxIntensity">
            <surfRegionList>
              <surfRegion name="surf"/>
            </surfRegionList>
          </surfElemResult>
          <surfRegionResult type="heatFlux">
            <surfRegionList>
              <surfRegion name="surf" outputIds="txt"/>
            </surfRegionList>
          </surfRegionResult>
        </storeResults>
      </heatConduction>
    </pdeList>  
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
            <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

</cfsSimulation>

