<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Monopole on L-shaped domain using nonmatching grids</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2010-02-01</date>
    <keywords>
      <keyword>mortar fem</keyword>
      <keyword>nonmatching grids</keyword>
      <keyword>lagrange multiplier</keyword>
      <keyword>pml</keyword>
    </keywords>
    <references>
      Simon Triebenbacher, Master Thesis, 2006, section 5.2
      Rossing, Handbook of Acoustics, 2007, page 75ff.
      Morse and Ingard, Theoretical Acoustics, 1986, page 356ff.
    </references>
    <isVerified>yes</isVerified>
    <description>
      In this example the radiation from a cyrindrical source is simulated in
      a L-shaped 2D domain. The inner square (0.5 x 0.5 m^2) area of the domain
      is discretized with 15x15 bilinear quadrilaterals. The outer L-shaped 
      subdomain extends the inner square by 0.5 metres in each direction. In
      every direction we discretize with 10 biquadratic elements. The interface
      between the subdomains is of mortar type and the standard lagrange
      multiplier is used. For free-field radiation we apply a PML layer. In the
      file 'l2d_master_thesis.h5' the original linear grid and reference results
      may be found.
      
      The following piece of Python code can be used within a ParaView
      Programmable Filter to compute the analytical amplitude solution of this
      setup by making use of a Hankel function from SciPy.
      
      import scipy as sp
      from scipy import special
      
      # Dummy function for structured grids
      def flatten(input, output):
          # Copy the cells etc.
          output.ShallowCopy(input)
          newPoints = vtk.vtkPoints()
          numPoints = input.GetNumberOfPoints()
          for i in range(0, numPoints):
              coord = input.GetPoint(i)
              x, y, z = coord[:3]
              x = x * 1
              y = y * 1
              z = 3 + 0.5*z
              newPoints.InsertPoint(i, x, y, z)
          output.SetPoints(newPoints)
      
      def compute_analytical_sol(input, output):
          # Copy the cells etc.
          output.ShallowCopy(input)
          newPoints = vtk.vtkPoints()
          numPoints = input.GetNumberOfPoints()
          values = input.GetPointData().GetScalars()
          ca = vtk.vtkFloatArray()
          ca.SetName('analyticalSolution')
          ca.SetNumberOfComponents(1)
          ca.SetNumberOfTuples(numPoints)
          output.GetPointData().AddArray(ca)
      
          for i in range(0, numPoints):
              coord = input.GetPoint(i)
              x, y, z = coord[:3]
              x = x * 1
              y = y * 1
              z = z * 1
              newPoints.InsertPoint(i, x, y, z)
              bulkModulus = 1.156e5
              density = 1.0
              c = sqrt(bulkModulus/density)
              freq = 119.3662
              pi = 3.14159
              w = 2*pi*freq
              fac = 0.25 * complex(0,1)
              r = sqrt(x*x + y*y)
              ca.SetValue(i, abs(fac * special.hankel1(0, w/c * r) ) )
          output.SetPoints(newPoints)
      
      input = self.GetInputDataObject(0, 0)
      output = self.GetOutputDataObject(0)
      
      if input.IsA("vtkMultiBlockDataSet"):
          output.CopyStructure(input)
          iter = input.NewIterator()
          iter.UnRegister(None)
          iter.InitTraversal()
          while not iter.IsDoneWithTraversal():
              curInput = iter.GetCurrentDataObject()
              curOutput = curInput.NewInstance()
              curOutput.UnRegister(None)
              output.SetDataSet(iter, curOutput)
              compute_analytical_sol(curInput, curOutput)
              iter.GoToNextItem();
      else:
          flatten(input, output)

      Whereby the the following SciPy Code (cf. SciPy special functions docu
      (http://docs.scipy.org/doc/scipy-0.7.x/reference/tutorial/special.html), 
      Treatment of complex numbers in Python
      (http://docs.python.org/tutorial/introduction.html))
      
       import scipy as sp
       from scipy import special
       special.hankel1(0,1)
      (0.76519768655796638+0.088256964215676997j)

      corresponds to the following Matlab/Octave code
      
      octave:1 besselh(0,1,1)
      ans =  0.765198 + 0.088257i      
    </description>
  </documentation>

  <fileFormats>
    <input>
      <hdf5 fileName="l2d_inner.h5" id="inner"
        readEntities="inner inner_iface1 inner_iface2"
        linearizeEntities="__all__"/>
      <gmsh fileName="l2d_outer.msh" id="outer"/>
      <!--hdf5 fileName="l2d_master_thesis.h5" id="msth"/-->
    </input>
    
    <output>
      <hdf5 id="hdf5"/>
    </output>
    
    <materialData file="mat.xml"/>
  </fileFormats>
  
  <domain geometryType="plane">

    <regionList>
      <region name="inner" material="air"/>
      <region name="outer" material="air"/>
      <region name="pml" material="air"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="inner_iface1"/>
      <surfRegion name="outer_iface1"/>
      <surfRegion name="inner_iface2"/>
      <surfRegion name="outer_iface2"/>
      <surfRegion name="absbc"/>
    </surfRegionList>

    <ncInterfaceList>
      <ncInterface name="ncIface1" masterSide="inner_iface1" slaveSide="outer_iface1"/>
      <ncInterface name="ncIface2" masterSide="inner_iface2" slaveSide="outer_iface2"/>
    </ncInterfaceList>
    
    <nodeList>
      <nodes name="load">
        <coord x="0.0" y="0.0" z="0.0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <Lagrange>
      <gridOrder/>
    </Lagrange>
  </fePolynomialList>
  
  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>    1   </numFreq>
        <startFreq>  119.3662 </startFreq>
        <stopFreq>   119.3662 </stopFreq>
      </harmonic>
    </analysis>
    
    <pdeList>
      <acoustic formulation="acouPotential">

        <regionList>
          <region name="inner"/>
          <region name="outer"/>
          <region name="pml" dampingId="myPML"/>
        </regionList>

        <ncInterfaceList>
          <ncInterface name="ncIface1" formulation="Mortar"/>
          <ncInterface name="ncIface2" formulation="Mortar"/>
        </ncInterfaceList>
        
        <dampingList>
          <pml id="myPML">
            <type>inverseDist</type>
            <dampFactor> 1 </dampFactor>
          </pml>          
        </dampingList>
        
        <bcsAndLoads>
          <potential name="load" value="1"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="acouPotential">
            <regionList>
              <region name="inner"/>
              <region name="outer"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <setup idbcHandling="penalty"/>
            <exportLinSys/>
            <matrix reordering="noReordering" storage="sparseNonSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct> no </symStruct>
            <IterRefineSteps>100</IterRefineSteps>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
