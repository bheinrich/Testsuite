<?xml version='1.0' encoding='utf-8'?>
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material">
  <!-- the dynamic behaviour is roughly similar to alumninum. The eigenfrequencies are of roughly similar order, the damping is the same. -->
  <material name="99lines">
    <mechanical>
      <density>
        <linear>
          <real>1e-8</real>
        </linear>
      </density>
      <!-- Data as in the 99-lines code by Sigmund -->
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>1</real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <lossTangensDelta>0.03</lossTangensDelta>
          <measuredFreq>1000</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
    <heatConduction>
      <!-- Data obtained from : "http://www.yutopian.com/Yuan/prop/"
           Reference(s) : "www.yutopian.com/Yuan/prop/" -->
      <!-- Remarks : "@ 27 degree c" Air -->
      <density>
        <linear>
          <real> 1.18 </real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real>  1005 </real>
        </linear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real> 1.0 </real>
          </isotropic>
        </linear>
      </heatConductivity>
    </heatConduction>
  </material>
  <!-- A little less than 99lines -->
  <material name="soft">
    <mechanical>
      <density>
        <linear>
          <real>1e-9</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>1e-2</real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <lossTangensDelta>0.03</lossTangensDelta>
          <measuredFreq>1000</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
  </material>
  <!-- similar to scaled down base constranst -->
  <material name="base_contrast">
    <mechanical>
      <density>
        <linear>
          <real>1e-8</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>0.01</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="low_contrast">
    <mechanical>
      <density>
        <linear>
          <real>1e-6</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>1</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="high_contrast">
    <mechanical>
      <density>
        <linear>
          <real>1e-5</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>10</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <!-- zero Poisson's ratio-->
  <material name="base_contrast_zero">
    <mechanical>
      <density>
        <linear>
          <real>1e-8</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>0.01</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0</real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="low_contrast_zero">
    <mechanical>
      <density>
        <linear>
          <real>1e-6</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>1</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0</real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="high_contrast_zero">
    <mechanical>
      <density>
        <linear>
          <real>1e-5</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>10</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0</real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <!-- Multimaterial orientation optimization.
  Base material: cross 40% and 10% of 1/0.3 core material 
  x   y 11           12           22           33
  40  10 2.021953e+00 9.486268e-02 6.151248e-01 1.475974e-02
  Note, that CFS by default rotates 3D, which means swapping y and z.
  BUT! If we read the material as multimaterial there is not such a rotation! 
   -->
  <material name="orient_0_degrees">
    <mechanical>
      <density>
        <linear>
          <real>1000</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            2.022       0.0949      0.0949      0.00000E+00 0.00000E+00 0.00000E+00
            0.0949      0.615       0.0949      0.00000E+00 0.00000E+00 0.00000E+00
            0.0949      0.0949      0.615       0.00000E+00 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.0148      0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.0148      0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.0148     
          </real>
          </tensor>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="orient_90_degrees">
    <mechanical>
      <density>
        <linear>
          <real>1000</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            0.615       0.0949      0.0949      0.00000E+00 0.00000E+00 0.00000E+00
            0.0949      2.022       0.0949      0.00000E+00 0.00000E+00 0.00000E+00
            0.0949      0.0949      0.615       0.00000E+00 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.0148      0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.0148      0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.0148     
          </real>
          </tensor>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="orient_135_degrees">
    <mechanical>
      <density>
        <linear>
          <real>1000</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            0.72150  0.69190  0.69190 -0.35175 -0.35175 -0.35175 
            0.69190  0.72150  0.69190 -0.35175 -0.35175 -0.35175
            0.69190  0.69190  0.72150 -0.35175 -0.35175 -0.35175
           -0.35175 -0.35175 -0.35175  0.6118  -0.35175 -0.35175
           -0.35175 -0.35175 -0.35175 -0.35175  0.6118  -0.35175
           -0.35175 -0.35175 -0.35175 -0.35175 -0.35175  0.6118
          </real>
          </tensor>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="orient_45_degrees">
    <mechanical>
      <density>
        <linear>
          <real>1000</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            0.72150  0.69190  0.69190  0.35175  0.35175  0.35175 
            0.69190  0.72150  0.69190  0.35175  0.35175  0.35175
            0.69190  0.69190  0.72150  0.35175  0.35175  0.35175
            0.35175  0.35175  0.35175  0.6118   0.35175  0.35175
            0.35175  0.35175  0.35175  0.35175  0.6118   0.35175
            0.35175  0.35175  0.35175  0.35175  0.35175  0.6118
          </real>
          </tensor>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="PolycrystallineRefractory">
    <mechanical>
      <density>
        <linear>
          <real>1</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>3.3e9</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0.22</real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="silizium">
    <mechanical>
      <density>
        <linear>
          <real> 2.3e3 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            1.65682E+11 1.84091E+10 1.84091E+10 0.00000E+00 0.00000E+00 0.00000E+00
            1.84091E+10 1.65682E+11 1.84091E+10 0.00000E+00 0.00000E+00 0.00000E+00
            1.84091E+10 1.84091E+10 1.65682E+11 0.00000E+00 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 7.36364E+10 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 7.36364E+10 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 7.36364E+10
          </real>
          </tensor>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="PolycrystallineRefractoryScaled">
    <mechanical>
      <density>
        <linear>
          <real>1.1</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real>3.3</real>
            </elasticityModulus>
            <poissonNumber>
              <real>0.22</real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="TitaniumWikipedia">
    <mechanical>
      <!-- Data obtained from en.wikipedia.org -->
      <density>
        <linear>
          <real> 4506 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 110e9 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.32 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <lossTangensDelta>0.02</lossTangensDelta>
          <measuredFreq>1</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
  </material>
  <material name="TitaniumWikipediaScaled">
    <mechanical>
      <!-- Data obtained from en.wikipedia.org -->
      <density>
        <linear>
          <real> 4506 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 1</real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.32 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <lossTangensDelta>0.02</lossTangensDelta>
          <measuredFreq>1</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
  </material>
  <material name="sigmund">
    <mechanical>
      <density>
        <linear>
          <real> 1 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 0.8615 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.23076 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="sigmund_void">
    <mechanical>
      <density>
        <linear>
          <real> 1 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 0.00086 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.23076 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
  <material name="Steel">
    <mechanical>
      <!-- Data obtained from : "http://www.matweb.com"
           Reference(s) : "www.matweb.com"
           Remarks : "AISI 1005" -->
      <density>
        <linear>
          <real> 7872 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 2.00E+11 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.29 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <alpha>1E+04</alpha>
          <beta>1E-08</beta>
          <measuredFreq>1E6</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
    <acoustic>
      <!-- Data obtained from : "http://www.ondacorp.com"
           Reference(s) :  -->
      <density>
        <linear>
          <real> 7800 </real>
        </linear>
      </density>
      <compressionModulus>
        <linear>
          <real> 2.7152E+11 </real>
        </linear>
      </compressionModulus>
    </acoustic>
  </material>
  <material name="aluminium">
    <mechanical>
      <density>
        <linear>
          <real>2.70031E+03</real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            1.07800E+11 5.49300E+10 5.49300E+10 0.00000E+00 0.00000E+00 0.00000E+00
            5.49300E+10 1.07800E+11 5.49300E+10 0.00000E+00 0.00000E+00 0.00000E+00
            5.49300E+10 5.49300E+10 1.07800E+11 0.00000E+00 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 2.64500E+10 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 2.64500E+10 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 2.64500E+10
          </real>
          </tensor>
        </linear>
      </elasticity>
      <damping>
        <rayleigh>
          <lossTangensDelta>0.03</lossTangensDelta>
          <measuredFreq>1000</measuredFreq>
        </rayleigh>
      </damping>
    </mechanical>
  </material>
  <material name="water">
    <heatConduction>
      <!-- Data obtained from : "http://http://thermopedia.com/content/1254/"-->
      <!-- Remarks : "@ 21 degree c" -->
      <density>
        <linear>
          <real> 1.18 </real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real> 4186 </real>
        </linear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real> 0.606 </real>
          </isotropic>
        </linear>
      </heatConductivity>
    </heatConduction>
  </material>
  <material name="air">
    <acoustic>
      <!-- Data obtained from : "http://www.ondacorp.com"
           Reference(s) : "Handbook of Chemistry and Physics, 45th Edition" -->
      <!-- Remarks : "dry air @ 20 degree c" -->
      <density>
        <linear>
          <real> 1.29 </real>
        </linear>
      </density>
      <compressionModulus>
        <linear>
          <real> 1.527E+05 </real>
        </linear>
      </compressionModulus>
    </acoustic>
    <electric>
      <!-- Data obtained from : "http://hyperphysics.phy-astr.gsu.edu/hbase/hframe.html"
           Reference(s) : "Sears, F. W., Zemansky, M. W., Young, H. D., University Physics, 6th Ed., Addison-Wesley, 1982."
           Remarks : "@20 degree centigrade, and 1 atm" -->
      <permittivity>
        <linear>
          <isotropic>
            <real> 8.859E-12 </real>
          </isotropic>
        </linear>
      </permittivity>
    </electric>
    <heatConduction>
      <!-- Data obtained from : "http://www.yutopian.com/Yuan/prop/"
           Reference(s) : "www.yutopian.com/Yuan/prop/" -->
      <!-- Remarks : "@ 27 degree c" -->
      <density>
        <linear>
          <real> 1.18 </real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real>  1005 </real>
        </linear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real> 0.03 </real>
          </isotropic>
        </linear>
      </heatConductivity>
    </heatConduction>
    <magnetic>
      <!-- Data obtained from : "http://www.matweb.com"
           Reference(s) : "Conductivity: www.matweb.com; Permeability: http://www.ee.surrey.ac.uk/Workshop/advice/coils/mu/#mu0"
           Remarks : "Name: K0.5Na0.5Nb03, air fired" -->
      <electricConductivity>
        <linear>
          <isotropic>
            <real>1.0000E-10</real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.2566E-06 </real>
          </isotropic>
        </linear>
      </permeability>
    </magnetic>
  </material>
</cfsMaterialDataBase>
